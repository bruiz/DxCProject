﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using TestApp.Services.Abstract;
using TestApp.Services.Concrete;



namespace ConsoleApp1.Infrastructure
{
    public class NinjectDependencyResolver 
    {
        private static IKernel kernel;

        public static IKernel Kernel
        {
            get { return kernel; }
        }

        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            addBindings(kernel);
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void addBindings(IKernel kernel)
        {
          kernel.Bind<IWService>().To<WService>();
        }
    }

}