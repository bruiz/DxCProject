﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Model;
using TestApp.Services.DataTransferObjects;
using AutoMapper;

namespace ConsoleApp1.Infrastructure
{
    public static class AutoMapperWebConfiguration
    {
        /// <summary>
        /// Creates all maps between view models classes and services entities or domain models.
        /// </summary>
        public static void Configure()
        {
            configureMapping();
        }


        private static void configureMapping()
        {
            Mapper.CreateMap<DTOWord, Word>();
        }
    }
}

