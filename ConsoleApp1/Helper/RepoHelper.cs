﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Services.Abstract;
using System.Configuration;
using TestApp.Services.Concrete;
using TestApp.Domain.Abstract;
using TestApp.Domain.Concrete;

namespace TestApp.Helper
{
    public class RepoHelper
    {
        private IWService _iWService;

        public RepoHelper(IWService iWService)
        {
            this._iWService = iWService;
        }

        public IRepo GetRepo()
        {
            var repoType = ConfigurationManager.AppSettings["RepoType"];

            if (repoType == "File")
                return new FileRepo(_iWService);
            else if(repoType == "Db")
                return new DbRepo(_iWService);

            return null;
        }
    }
}
