﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Model
{
    public class Word
    {
        public string WordName;
        public int Size
        {
            get { return WordName.Length; }
        }
    } 
}
