﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Services.Abstract;
using TestApp.Services.Concrete;
using TestApp.Model;
using AutoMapper;
using TestApp.Services.DataTransferObjects;
using System.Configuration;
using TestApp.Helper;


namespace TestApp.Bussiness
{
    public class WordLogic
    {
        private IWService _iWService;

        public WordLogic(IWService iWService)
        {
            this._iWService = iWService;
        }

        public int AddWords()
        {
            var repo = new RepoHelper(_iWService);
            return repo.GetRepo().ReadData();
        }

        public IEnumerable<Word> GetWords()
        {
            var dto = _iWService.GetWords();
            var wordsList = Mapper.Map<IEnumerable<DTOWord>, IEnumerable<Word>>(dto);
            return wordsList;
        }

        public void PrintWords()
        {
            var maxLenght = int.Parse(ConfigurationManager.AppSettings["MaxLenght"]);
            var list = GetWords();
            foreach (Word item in list.ToList())
            {
                var wordLength = item.Size;
                if (wordLength < maxLenght)
                {
                    var auxList = _iWService.GetWordsbyLenght(maxLenght - wordLength);

                    foreach (var auxword in auxList.ToList())
                    {
                        var finalWord = item.WordName + auxword.WordName;
                        if (list.Any(x => x.WordName == finalWord))
                                Console.WriteLine(finalWord);
                    }   

                }
            }
        }

    }
}
