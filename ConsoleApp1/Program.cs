﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.Infrastructure;
using TestApp.Bussiness;
using TestApp.Services.Abstract;
using TestApp.Services.Concrete;



namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {

            var ninject = new NinjectDependencyResolver();
            AutoMapperWebConfiguration.Configure();

            var iWService = (IWService)ninject.GetService(typeof(IWService));
            WordLogic wl = new WordLogic(iWService);
            wl.AddWords();
            wl.PrintWords();
           
        }
    }
}
