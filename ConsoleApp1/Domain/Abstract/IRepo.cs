﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Domain.Abstract
{
    public interface IRepo
    {
        int ReadData();
    }
}
