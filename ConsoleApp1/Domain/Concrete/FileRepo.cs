﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Domain.Abstract;
using TestApp.Services.Abstract;

namespace TestApp.Domain.Concrete
{
    public class FileRepo : IRepo
    {

        private IWService _iWService;

        public FileRepo(IWService iWService)
        {
            this._iWService = iWService;
        }
        public int ReadData()
        {
            int counter = 0;
            string line;
            System.IO.StreamReader file =
                new System.IO.StreamReader(@"c:\test\wordlist.txt");

            while ((line = file.ReadLine()) != null)
            {
                _iWService.AddWord(line);
                counter++;
            }
            file.Close();
            return counter;
        }
    }
}
