﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Domain.Abstract;
using TestApp.Services.Abstract;

namespace TestApp.Domain.Concrete
{
    public class DbRepo : IRepo
    {

        private IWService _iWService;
        private DBModel ctx = new DBModel();

        public DbRepo(IWService iWService)
        {
            this._iWService = iWService;
        }

        public int ReadData()
        {
            int counter = 0;
            var words = ctx.DbWord.ToList();
            foreach(var word in words)
            {
                _iWService.AddWord(word.Name);
                counter++;
            }

            return counter;
        }
    }
}
