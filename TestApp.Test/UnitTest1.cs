﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestApp.Bussiness;
using TestApp.Services.Abstract;
using TestApp.Services.Concrete;
using Moq;

namespace TestApp.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ReadWords()
        {
            var mock = new Mock<IWService>();
            var wObject = new WordLogic(mock.Object);
            var wordCounter = wObject.AddWords();
            Assert.IsTrue(wordCounter > 0);
        }

        [TestMethod]
        public void GetWords()
        {
            var sService = new WService();
            sService.AddWord("Zomba");
            sService.AddWord("jailing");
            sService.AddWord("cassock");
            var wordList = sService.GetWords();
            Assert.IsNotNull(wordList);
        }

        [TestMethod]
        public void WordExist()
        {
            var sService = new WService();
            sService.AddWord("zoologically");
            Assert.IsTrue(sService.WordExist("zoologically"));
        }

    }
}
