﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Services.DataTransferObjects;

namespace TestApp.Services.Abstract
{
    public interface IWService
    {

        void AddWord(string word);

        IEnumerable<DTOWord> GetWordsbyLenght(int lenght);

        IEnumerable<DTOWord> GetWords();

        bool WordExist(string word);

    }
}
