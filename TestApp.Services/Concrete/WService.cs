﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Services.Abstract;
using TestApp.Services.DataTransferObjects;

namespace TestApp.Services.Concrete
{
    public class WService : IWService
    {
        List<DTOWord> wordList = new List<DTOWord>();

        public void AddWord(string word)
        {
            DTOWord newW = new DTOWord();
            newW.WordName = word;

            wordList.Add(newW);
        }

        public IEnumerable<DTOWord> GetWordsbyLenght(int length)
        {
            return wordList.Where(x => x.Size == length);
        }

        public IEnumerable<DTOWord> GetWords()
        {
            return wordList;
        }

        public bool WordExist(string word)
        {
            return wordList.Any(x => x.WordName == word);
        }


    }
}
