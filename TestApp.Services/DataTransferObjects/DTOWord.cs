﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Services.DataTransferObjects
{
    public class DTOWord
    {
        public string WordName;
        public int Size
        {
            get { return WordName.Length; }
        }
    }
}
